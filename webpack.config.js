/**
 * Environment configurations to run the project as well as other preparation that each
 * user of this application should be aware of. This namespace handles the core setup
 * prior the application's bootstarping. - See further for other namespace branches to hsee
 * how this project is being configured.
 * @namespace Environment
 * @summary Environment setup
 */


/**
 * Webpack configuration is what bundles the entire application and
 * stores the output inside a directory from which it will serve
 * the content during development mode.
 * @requires path
 * @requires webpack
 * @requires poststylus
 * @namespace Environment.Webpack
 * @summary Bundling and server static content serving
 */


const path = require('path');
const webpack = require('webpack');
const poststylus = require('poststylus');


/**
 * There is an object that contains information about webpack;s live development
 * server. - That information might be reused for other configurations like
 * static content service. And therefore this is not coded internally
 * inside the webpacj config object. - This needs to be global in this file
 * fur further configurations.
 * @summary Development server configuration
 * @static
 * @constant
 * @type {Object}
 * @property {Number} port - Dev server port
 * @memberof Environment.Webpack
 */
const DEV_SERVER = {
    PORT: 8081,
};


/**
 * This function gets passed the current directory name and then it returns
 * a function which will accept the fallow up string value to be joined with.
 * In return this gets a path that can be used to get absolute paths for
 * configuring webpack. - Currying is implemented as a style for this
 * approach just in case another directory may be provided from the system.
 * @summary Creates an entry function
 * @param {String} dirname  - Current directory
 * @static
 * @function
 * @memberof Environment.Webpack
 * @returns {Function}
 */
const entryCreator = dirname => pathname => path.join(dirname, pathname);


/**
 * Entry function is the returned function by
 * {@link Environment.Webpack.entryCreator} which allows to access prefixed path's
 * directory file system in a more convenient way.
 * @summary Creates an entry path from current directory
 * @param {String} pathname - Path
 * @static
 * @function
 * @memberof Environment.Webpack
 * @returns {String}
 */
const entry = entryCreator(__dirname);


/**
 * This function takes a foundational prefix to which further URl extensions
 * can be applied to. Hence the curried style which allows to do so.
 * @summary HTTP url prefixer
 * @param {String} prefix - Prefix URL
 * @static
 * @function
 * @memberof Environment.Webpack
 * @returns {Function}
 */
const httpCreator = prefix => url => prefix + url;


/**
 * This prefixer takes an extension, and all will be derived from the
 * local development server on a specified port which is declared inside
 * the {@link Environment.Webpack.DEV_SERVER} object.
 * @summary HTTP prefixer for development server
 * @param {String} url - URL
 * @static
 * @function
 * @memberof Environment.Webpack
 * @returns {String}
 */
const http = httpCreator(`http://localhost:${DEV_SERVER.PORT}`);


/**
 * Webpack module loading processing consists of a collection of lodaers
 * inside the rule property. - This function will return the passed in object
 * but with minor modifications what are obvious in it's declaration. - This
 * helps to remove duplication and makes it cleaner by isolating all common
 * desires in one place.
 * @summary Rule declaration utility function
 * @param {Object} config - Rule configuration
 * @static
 * @function
 * @memberof Environment.Webpack
 * @returns {Object}
 */
const rule = config => Object.assign(config, {
    exclude: [
        /bundles/,
        /node_modules/i,
        /mochawesome-report/,
    ],
});



/**
 * This is the configured object that gets imported by webpack and preform
 * the bundling and other specified actions within this file.
 * @summary Initial webpack configuration
 * @name Webpack Configuration Object
 * @alias webpack_config
 * @static
 * @constant
 * @type {Object}
 * @property {Object} entry - File system entry poinyt
 * @property {Object} output - Bundle output configuration
 * @property {Object} module - Module configuration
 * @property {Object} devServer - Development server configuration
 * @property {Object} resolve - Bundling resolvement configuration
 * @property {Array} plugins - Plugins used during bundling
 * @memberof Environment.Webpack
 */

const browser = {
    entry: {
        main: entry('./src/main.react.jsx'),
    },
    output: {
        path: entry('./bundles'),
        publicPath: http('/bundles'),
        filename: '[name].bundle.js',
    },
    module: {
        rules: [
            rule({ test: /\.jsx?$/i, loader: 'babel-loader' }),
            rule({ test: /\.styl$/, loader: 'style-loader!css-loader?modules=true!stylus-loader' }),
        ],
    },
    devServer: {
        port: DEV_SERVER.PORT,
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json'],
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            options: {
                stylus: {
                    use: [
                        poststylus(['autoprefixer', 'rucksack-css']),
                    ],
                },
            },
        }),
    ],
    devtool: 'cheap-module-source-map',
};


const server = {
    entry: {
        server: entry('./server/index.js'),
    },
    output: {
        path: entry('./bundles'),
        publicPath: entry('./bundles'),
        filename: '[name].bundle.js',
        libraryTarget: 'commonjs2',
    },
    module: {
        rules: [
            rule({ test: /\.jsx?$/i, loader: 'babel-loader' }),
            // rule({ test: /\.styl$/, loader: 'css-loader/locals' }),
        ],
    },
    node: {
        __dirname: true,
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.browser': true
        })
    ],
    resolve: {
        extensions: ['.js', '.jsx', '.json'],
    },
    target: 'node',
    devtool: 'cheap-module-source-map',
};


module.exports = browser;