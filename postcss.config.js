/**
 * Postcss is responsible for CSS compatability prefixing as well as other optimizations.
 * These required procedures are outlined in this configuration
 * as part of environment's configurational build tools.
 * @namespace Environment.Postcss
 * @summary Postcss Configuration
 */

module.exports = {
    parser: 'sugarss',
    plugins: {
        'cssnano': {},
        'postcss-import': {},
        'postcss-cssnext': {},
    },
};