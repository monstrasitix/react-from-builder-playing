// Core
import ReduxThunk from 'redux-thunk';
import { reducer } from 'redux-form';
import { createLogger } from 'redux-logger';
import { createStore, combineReducers, applyMiddleware } from 'redux';

// Reducers
import ControlReducer from './reducers/control.reducer';

/**
 * RThe store is required to contain the data that is conceptually
 * thought the be the source of truth when managing the data. - Store
 * is in infrastructure that contains the data nd manages incoming
 * inserts and updates than change the data throug middleware.
 * Optional/custom middleware can be assigned to this process for extra
 * leverage.
 * @summary Redux's store setup
 * @namespace Source.Store
 */


/**
 * The store data mutations are done through reducers which are divided
 * by choice of the application. - This object contains all available reducers
 * that will be used in action to preform updates to the store and portions of state
 * they are responsible for.
 * @summary Runtime available reducer functions
 * @static
 * @type {Object}
 * @property {Function} form - Redux-Form reducer
 * @property {Function} control - Control reducer
 * @memberof Source.Store
 */
export const storeReducers = {
    form: reducer,
    control: ControlReducer,
};


/**
 * Middleware are functions that stand in between the transactional procedure
 * of mutating the state. Whenever a change is being done redux will preform it's duty. While
 * that is being taken place all middleware listen here will capture that transaction
 * and preform their analysis over it and preform what ever is required to be accomplished
 * by it.
 * @summary Used middleware
 * @static
 * @type {Array}
 * @memberof Source.Store
 */
export const middleware = [
    ReduxThunk,
    createLogger({ collapsed: true })
];


export default createStore(
    combineReducers(storeReducers),
    applyMiddleware(...middleware),
);