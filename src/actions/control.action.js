// Utils
import { createAction } from '../utils/action.util';

export const actions = {
    CONTROL_ADD: 'CONTROL_ADD',
    CONTROL_SORT: 'CONTROL_SORT',
    CONTROL_UPDATE: 'CONTROL_UPDATE',
    CONTROL_REMOVE: 'CONTROL_REMOVE',
    CONTROL_REPLACE: 'CONTROL_REPLACE',
    CONTROL_CURRENT_UPDATE: 'CONTROL_CURRENT_UPDATE',
};



export const controlAdd = createAction(actions.CONTROL_ADD);
export const controlSort = createAction(actions.CONTROL_SORT);
export const controlUpdate = createAction(actions.CONTROL_UPDATE);
export const controlRemove = createAction(actions.CONTROL_REMOVE);
export const controlReplace = createAction(actions.CONTROL_REPLACE);
export const controlCurrentUpdate = createAction(actions.CONTROL_CURRENT_UPDATE);