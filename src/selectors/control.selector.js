export const created = state =>
    state.control.createdControls;

export const currentId = state =>
    state.control.currentControlId;

export const currentControl = state => {
    const id = currentId(state);
    return created(state).find(control => control.id === id);
}

export const availableMapper = ([type, { title, icon }]) => ({
    type,
    icon,
    title,
});

export const available = controls =>
    Object.entries(controls).map(availableMapper);