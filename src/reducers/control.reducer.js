// Core
import { handleActions } from 'redux-actions';

// Actions
import { actions } from '../actions/control.action';


export const initialState = {
    createdControls: [],
    currentControlId: undefined,
};

export const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};


export default handleActions({
    [actions.CONTROL_ADD]: (state, { payload }) => ({
        ...state,
        createdControls: [...state.createdControls, payload.data],
    }),
    [actions.CONTROL_CURRENT_UPDATE]: (state, { payload }) => ({
        ...state,
        currentControlId: payload,
    }),
    [actions.CONTROL_REMOVE]: (state, { payload }) => ({
        ...state,
        createdControls: state.createdControls.filter(
            control => control.id !== payload
        ),
    }),
    [actions.CONTROL_REPLACE]: (state, { payload }) => ({
        ...state,
        createdControls: payload,
    }),
    [actions.CONTROL_SORT]: (state, { payload: { sourceIndex, destinationIndex } }) => (console.log(sourceIndex, destinationIndex), {
        ...state,
        createdControls: reorder(state.createdControls, sourceIndex, destinationIndex),
    }),
    [actions.CONTROL_UPDATE]: (state, action) => ({
        ...state,
        createdControls: state.createdControls.map((control) => {
            if (control.id === action.payload.id) {
                control.settings = {
                    ...control.settings,
                    ...action.payload.values,
                };
            }
            return control;
        }),
    }),
}, initialState);