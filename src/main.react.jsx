// Stylus
import './assets/stylus/main.styl';

// Core
import React from 'react';
import { render } from 'react-dom';

// Store
import Store from './store';

// Components
import Root from './components/Root.react';


/**
 * Source is the initial project location which contains other namespaces and it
 * contained modules revolving around React's deveopment stack.
 * @namespace Source
 * @summary Source project
 */


/**
 * This is the initial root of the entrier appication as far as
 * Webpack bundling goes and initial rendering of the application.
 * @requires React
 * @requires ReactDOM
 * @requires Source.Store
 * @requires Source.Components.Root
 * @namespace Source.Main
 * @summary Initialisation
 */


render(
    <Root store={Store} />,
    document.getElementById('app'),
);