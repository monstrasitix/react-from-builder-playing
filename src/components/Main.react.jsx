// Core
import React from 'react';
import PropTypes from 'prop-types';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

// Components
import Tabs from './Tabs.react';
import Card from './Card.react';
import Custom from './Custom.react';
import TabContents from './TabContents.react';
import { CONTROLS } from './controls/controls.react';
import AvailableControlPreview from './AvailableControlPreview.react';
import ControlConfigurationPreview from './ControlConfigurationPreview.react';

// Containers
import ControlSettingsContainer from '../containers/ControlSettingsContainer.react';

const Main = ({
    tabs,
    type,
    onDragEnd,
    droppableIds,
    currentControl,
    createdControls,
    onControlChange,
    availableControls,
}) => (
        <div className="container">
            <div className="row mt-5">
                <DragDropContext onDragEnd={onDragEnd}>
                    <div className="col-6">
                        <Card>
                            <Tabs tabs={tabs} />

                            <TabContents
                                tabs={tabs}
                                className="mt-2"
                                content={{
                                    [tabs[0].id]: (
                                        <AvailableControlPreview
                                            type={type}
                                            droppableId={droppableIds.available}
                                            availableControls={availableControls}
                                        />
                                    ),
                                    [tabs[1].id]: (
                                        <ControlSettingsContainer
                                            currentControl={currentControl}
                                        />
                                    ),
                                }}
                            />
                        </Card>
                    </div>

                    <div className="col-6">
                        <ControlConfigurationPreview
                            type={type}
                            droppableId={droppableIds.created}
                            createdControls={createdControls}
                            onControlChange={onControlChange}
                        />
                    </div>
                </DragDropContext>
            </div>
        </div>
    );


Main.propsTypes = {
    type: PropTypes.string,
    onDragEnd: PropTypes.func,
    onControlChange: PropTypes.func,
    currentControl: PropTypes.object,
    createdControls: PropTypes.array,
    availableControls: PropTypes.array,
    droppableIds: PropTypes.object.isRequired,
    tabs: PropTypes.arrayOf(PropTypes.object).isRequired,
};


Main.defaultProps = {
    type: 'DEFAULT',
    currentControl: {},
    createdControls: [],
    availableControls: [],
    onDragEnd: console.warn.bind(console, 'onDragEnd is not implemented in MainContainer'),
    onControlChange: console.warn.bind(console, 'onControlChange is not implemented in MainContainer'),
};


export default Main;