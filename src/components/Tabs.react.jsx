// Core
import React from 'react';
import PropTypes from 'prop-types';

// Components
import Tab from './Tab.react';


export const tabMap = tab => (
    <Tab key={tab.id} {...tab} />
);


const Tabs = ({ tabs }) => (
    <ul className="nav nav-tabs nav-justified">
        {tabs.map(tabMap)}
    </ul>
);


Tabs.propTypes = {
    tabs: PropTypes.arrayOf(PropTypes.object),
};


Tabs.defaultProps = {
    tabs: [],
};

export default Tabs;