// Core
import React from 'react';
import PropTypes from 'prop-types';

// Components
import TabContent from './TabContent.react';
import Tab from './Tab.react';


export const tabMap = content => tab => (
    <TabContent key={tab.id} id={tab.id} isActive={tab.isActive}>
        {content[tab.id]}
    </TabContent>
);


const TabContents = ({ className, tabs, content }) => (
    <div className={`tab-content ${className}`}>
        {tabs.map(tabMap(content))}
    </div>
);


TabContents.propTypes = {
    content: PropTypes.object,
    className: PropTypes.string,
    tabs: PropTypes.arrayOf(PropTypes.object),
};


TabContents.defaultProps = {
    tabs: [],
    content: {},
    className: '',
};


export default TabContents;