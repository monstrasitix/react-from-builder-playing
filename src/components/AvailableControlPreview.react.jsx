// Core
import React from 'react';
import PropTypes from 'prop-types';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'


export const DraggableSection = control => (provided, snapshot) => (
    <div className="col-6">
        <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
        >
            <div className="card mt-3">
                <div className="card-body control" style={{ position: 'relative' }}>
                    <div className="control__drag" style={{height: '100%', width: '10%', backgroundColor: '#ededed', position: 'absolute', left: 0, top: 0}} />
                    <div className="d-flex align-items-center justify-content-center flex-column">
                        <i className={control.icon} />
                        <span>{control.title}</span>
                    </div>
                </div>
            </div>
        </div>
        {provided.placeholder}
    </div>
);


export const DraggableSectionMap = (control, index) => (
    <Draggable key={control.type} draggableId={control.type} index={index}>
        {DraggableSection(control)}
    </Draggable>
);


export const DroppableSection = availableControls => (provided, snapshot) => (
    <div className="container" >
        <div className="row" ref={provided.innerRef}>
            {availableControls.map(DraggableSectionMap)}
            {provided.placeholder}
        </div>
    </div>

);


const AvailableControlPreview = ({ availableControls, droppableId, type }) => (
    <Droppable droppableId={droppableId} type={type}>
        {DroppableSection(availableControls)}
    </Droppable>
);


AvailableControlPreview.propTypes = {
    type: PropTypes.string.isRequired,
    availableControls: PropTypes.array,
    droppableId: PropTypes.string.isRequired,
};


AvailableControlPreview.defaultProps = {
    availableControls: [],
};


export default AvailableControlPreview;