// Core
import React from 'react';
import PropTypes from 'prop-types';


const Custom = ({ component, attributes, children }) =>
    React.createElement(
        component,
        attributes,
        children,
    );



Custom.propTypes = {
    component: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.object
    ]).isRequired,
    attributes: PropTypes.object,
    children: PropTypes.node,
};


Custom.defaultProps = {
    children: '',
    attributes: {},
};


export default Custom;