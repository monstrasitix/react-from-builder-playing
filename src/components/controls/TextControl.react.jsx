// Core
import React from 'react';
import PropTypes from 'prop-types';

// Constants
import { autoCompleteOptions } from '../../constants/input.constant';


export const identifier = id => `text_${id}`;


const TextControl = ({ id, settings }) => (
    <div className="form-group">
        <label htmlFor={identifier(id)}>
            {settings.label}
        </label>
        <input
            id={id}
            type="text"
            name={identifier(id)}
            className="form-control"
            value={settings.value}
            pattern={settings.pattern}
            disabled={settings.disabled}
            readOnly={settings.readonly}
            required={settings.required}
            maxLength={settings.maxlength}
            minLength={settings.minlength}
            placeholder={settings.placeholder}
            autoComplete={settings.autocomplete}
        />
    </div>
);


TextControl.propTypes = {
    id: PropTypes.string.isRequired,
    settings: PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string,
        disabled: PropTypes.bool,
        readonly: PropTypes.bool,
        required: PropTypes.bool,
        pattern: PropTypes.string,
        maxlength: PropTypes.number,
        minlength: PropTypes.number,
        placeholder: PropTypes.placeholder,
        autocomplete: PropTypes.oneOf(autoCompleteOptions.values),
    }),
};


TextControl.defaultProps = {
    settings: {
        value: '',
        pattern: '',
        maxlength: 0,
        minlength: 0,
        disabled: false,
        readonly: false,
        required: false,
        label: 'Text label',
        placeholder: 'Placeholder',
        autocomplete: autoCompleteOptions.default,
    },
};

export default TextControl;