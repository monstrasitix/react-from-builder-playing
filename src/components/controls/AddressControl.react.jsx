// Core
import React from 'react';
import PropTypes from 'prop-types';

// Constants
import { autoCompleteOptions } from '../../constants/input.constant';


export const identifier = id => `text_${id}`;


const AddressControl = ({ id, settings }) => (
    <div className="form-group">
        <div className="row">
            <div className="form-group col-6">
                <label htmlFor={identifier(id)}>
                    {settings.label}
                </label>
                <input
                    id={id}
                    type="text"
                    name={identifier(id)}
                    className="form-control"
                    value={settings.value}
                    pattern={settings.pattern}
                    readOnly={settings.readOnly}
                    required={settings.required}
                    maxLength={settings.maxLength}
                    minLength={settings.minLength}
                    placeholder={settings.placeholder}
                    autoComplete={settings.autoComplete}
                />
            </div>
            <div className="form-group col-6">
                <label htmlFor={identifier(id)}>
                    {settings.label}
                </label>
                <input
                    id={id}
                    type="text"
                    name={identifier(id)}
                    className="form-control"
                    value={settings.value}
                    pattern={settings.pattern}
                    readOnly={settings.readOnly}
                    required={settings.required}
                    maxLength={settings.maxLength}
                    minLength={settings.minLength}
                    placeholder={settings.placeholder}
                    autoComplete={settings.autoComplete}
                />
            </div>
        </div>
        <label htmlFor={identifier(id)}>
            {settings.label}
        </label>
        <input
            id={id}
            type="text"
            name={identifier(id)}
            className="form-control"
            value={settings.value}
            pattern={settings.pattern}
            readOnly={settings.readOnly}
            required={settings.required}
            maxLength={settings.maxLength}
            minLength={settings.minLength}
            placeholder={settings.placeholder}
            autoComplete={settings.autoComplete}
        />
    </div>
);


AddressControl.propTypes = {
    id: PropTypes.string.isRequired,
    settings: PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string,
        disabled: PropTypes.bool,
        readOnly: PropTypes.bool,
        required: PropTypes.bool,
        pattern: PropTypes.string,
        maxLength: PropTypes.number,
        minLength: PropTypes.number,
        placeholder: PropTypes.placeholder,
        autoComplete: PropTypes.oneOf(autoCompleteOptions.values),
    }),
};


AddressControl.defaultProps = {
    settings: {
        value: '',
        pattern: '',
        maxLength: 0,
        minLength: 0,
        disabled: false,
        readOnly: false,
        required: false,
        label: 'Text label',
        placeholder: 'Placeholder',
        autoComplete: autoCompleteOptions.default,
    },
};

export default AddressControl;