// Core
import React from 'react';
import PropTypes from 'prop-types';

// Constants
import { autoCompleteOptions } from '../../constants/input.constant';

// Components
//// Controls
import TextControl from './TextControl.react';
import NumberControl from './NumberControl.react';
import DateControl from './DateControl.react';
import TimeControl from './TimeControl.react';
import EmailControl from './EmailControl.react';
import FileControl from './FileControl.react';
import CardNumberControl from './CardNumberControl.react';
import AddressControl from './AddressControl.react';
import PhoneControl from './PhoneControl.react';
import ChecklistControl from './ChecklistControl.react';
import RadiolistControl from './RadiolistControl.react';
import DropdownControl from './DropdownControl.react';
import UrlControl from './UrlControl.react';
import TogglerControl from './TogglerControl.react';


//// Settings
import TextControlSettings from '../settings/TextControlSettings.react';
import NumberControlSettings from '../settings/NumberControlSettings.react';
import DateControlSettings from '../settings/DateControlSettings.react';
import TimeControlSettings from '../settings/TimeControlSettings.react';
import EmailControlSettings from '../settings/EmailControlSettings.react';
import FileControlSettings from '../settings/FileControlSettings.react';
import CardNumberControlSettings from '../settings/CardNumberControlSettings.react';
import AddressControlSettings from '../settings/AddressControlSettings.react';
import PhoneControlSettings from '../settings/PhoneControlSettings.react';
import ChecklistControlSettings from '../settings/ChecklistControlSettings.react';
import RadiolistControlSettings from '../settings/RadiolistControlSettings.react';
import DropdownControlSettings from '../settings/DropdownControlSettings.react';
import UrlControlSettings from '../settings/UrlControlSettings.react';
import TogglerControlSettings from '../settings/TogglerControlSettings.react';


export const CONTROLS = {
    text: {
        title: 'Text',
        icon: 'fa fa-font',
        control: TextControl,
        settingsControl: TextControlSettings,
    },
    number: {
        title: 'Number',
        icon: 'fa fa-subscript',
        control: NumberControl,
        settingsControl: NumberControlSettings,
    },
    // date: {
    //     title: 'Date',
    //     icon: 'fa fa-calendar',
    //     control: DateControl,
    //     settingsControl: DateControlSettings,
    // },
    // time: {
    //     title: 'Time',
    //     icon: 'fa fa-clock-o',
    //     control: TimeControl,
    //     settingsControl: TimeControlSettings,
    // },
    // file: {
    //     title: 'File',
    //     icon: 'fa fa-upload',
    //     control: FileControl,
    //     settingsControl: FileControlSettings,
    // },
    // cardNumber: {
    //     title: 'Card Number',
    //     icon: 'fa fa-credit-card',
    //     control: CardNumberControl,
    //     settingsControl: CardNumberControlSettings,
    // },
    // email: {
    //     title: 'Email',
    //     icon: 'fa fa-envelope-o',
    //     control: EmailControl,
    //     settingsControl: EmailControlSettings,
    // },
    // address: {
    //     title: 'Address',
    //     icon: 'fa fa-address-card-o',
    //     control: AddressControl,
    //     settingsControl: AddressControlSettings,
    // },
    // phone: {
    //     title: 'Phone',
    //     icon: 'fa fa-phone',
    //     control: PhoneControl,
    //     settingsControl: PhoneControlSettings,
    // },
    // checklist: {
    //     title: 'Check List',
    //     icon: 'fa fa-check-square-o',
    //     control: ChecklistControl,
    //     settingsControl: ChecklistControlSettings,
    // },
    // radiolist: {
    //     title: 'Radio List',
    //     icon: 'fa fa-dot-circle-o',
    //     control: RadiolistControl,
    //     settingsControl: RadiolistControlSettings,
    // },
    // dropdown: {
    //     title: 'Dropdown List',
    //     icon: 'fa fa-list',
    //     control: DropdownControl,
    //     settingsControl: DropdownControlSettings,
    // },
    // url: {
    //     title: 'URL',
    //     icon: 'fa fa-globe',
    //     control: UrlControl,
    //     settingsControl: UrlControlSettings,
    // },
    // toggler: {
    //     title: 'Toggler',
    //     icon: 'fa fa-toggle-on',
    //     control: TogglerControl,
    //     settingsControl: TogglerControlSettings,
    // },
};