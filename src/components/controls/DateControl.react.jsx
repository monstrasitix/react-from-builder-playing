// Core
import React from 'react';
import PropTypes from 'prop-types';

export const identifier = id => `number_${id}`;


const DateControl = ({ id, settings }) => (
    <div className="form-group">
        <label htmlFor={identifier(id)}>
            {settings.label}
        </label>
        <input
            id={id}
            type="date"
            className="form-control"
            name={identifier(id)}
            value={settings.value}
            readOnly={settings.readOnly}
            required={settings.required}
            placeholder={settings.placeholder}
        />
    </div>
);


DateControl.propTypes = {
    id: PropTypes.string.isRequired,
    settings: PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string,
        disabled: PropTypes.bool,
        readOnly: PropTypes.bool,
        required: PropTypes.bool,
        placeholder: PropTypes.placeholder,
    }),
};


DateControl.defaultProps = {
    settings: {
        value: '',
        disabled: false,
        readOnly: false,
        required: false,
        label: 'Number label',
        placeholder: 'Placeholder',
    },
};

export default DateControl;