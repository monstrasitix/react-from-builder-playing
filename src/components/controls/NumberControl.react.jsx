// Core
import React from 'react';
import PropTypes from 'prop-types';

// Constants
import { autoCompleteOptions } from '../../constants/input.constant';


export const identifier = id => `number_${id}`;


const NumberControl = ({ id, settings }) => (
    <div className="form-group">
        <label htmlFor={identifier(id)}>
            {settings.label}
        </label>
        <input
            id={id}
            type="number"
            className="form-control"
            max={settings.max}
            min={settings.min}
            name={identifier(id)}
            step={settings.step}
            value={settings.value}
            readOnly={settings.readonly}
            required={settings.required}
            placeholder={settings.placeholder}
        />
    </div>
);


NumberControl.propTypes = {
    id: PropTypes.string.isRequired,
    settings: PropTypes.shape({
        max: PropTypes.number,
        min: PropTypes.number,
        step: PropTypes.number,
        value: PropTypes.string,
        label: PropTypes.string,
        disabled: PropTypes.bool,
        readonly: PropTypes.bool,
        required: PropTypes.bool,
        placeholder: PropTypes.placeholder,
    }),
};


NumberControl.defaultProps = {
    settings: {
        max: 0,
        min: 0,
        step: 1,
        value: '',
        disabled: false,
        readonly: false,
        required: false,
        label: 'Number label',
        placeholder: 'Placeholder',
    },
};

export default NumberControl;