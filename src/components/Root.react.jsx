// Core
import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

// Containers
import MainContainer from '../containers/MainContainer.react';


const Root = ({ store }) => (
    <Provider store={store}>
        <MainContainer />
    </Provider>
);


Root.propTypes = {
    store: PropTypes.object.isRequired,
};

Root.defaultProps = {};


export default Root;