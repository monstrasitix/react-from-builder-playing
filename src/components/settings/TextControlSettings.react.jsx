// Core
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, initialize } from 'redux-form';
import { connect } from 'react-redux';

// Constants
import { autoCompleteOptions } from '../../constants/input.constant';

// Components
import TextControl from '../controls/TextControl.react';
import InputField from '../configurable/InputField.react';
import SelectField from '../configurable/SelectField.react';
import CheckboxField from '../configurable/CheckboxField.react';

// Selectors
import { currentControl } from '../../selectors/control.selector';

// Utils
import { toNumber, toString } from '../../utils/normalize.util';

let TextControlSettings = () => (
    <div>
        <div className="form-group row">
            <InputField type="text" name="label" text="Label name" component="input" placeholder="Label's name" normalize={toString} />
        </div>
        <div className="form-group row">
            <InputField type="text" name="placeholder" text="Placeholder" component="input" placeholder="Text" normalize={toString} />
        </div>
        <div className="form-group row">
            <InputField type="text" name="pattern" text="Pattern" component="input" placeholder="Pattern" normalize={toString} />
        </div>
        <div className="form-group row">
            <InputField type="number" name="maxlength" text="Maximum length" component="input" placeholder="Maximum character length" normalize={toNumber} />
        </div>
        <div className="form-group row">
            <InputField type="number" name="minlength" text="Minimum length" component="input" placeholder="Minimum character length" normalize={toNumber} />
        </div>
        <div className="form-group row">
            <SelectField text="Autocomplete" name="autocomplete" options={autoCompleteOptions.values} />
        </div>
        <div className="form-group">
            <CheckboxField name="disabled" text="Disabled" />
            <CheckboxField name="readonly" text="Read Only" />
            <CheckboxField name="required" text="Required" />
        </div>
    </div>
);


TextControlSettings.propTypes = {};
TextControlSettings.defaultProps = {};


TextControlSettings = reduxForm({
    form: 'textSettingsForm',
    fields: Object.keys(TextControl.propTypes.settings),
})(TextControlSettings);


TextControlSettings = connect(
    state => ({
        initialValues: currentControl(state).settings,
    }),
)(TextControlSettings);



export default TextControlSettings;