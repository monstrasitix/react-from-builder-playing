// Core
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, initialize } from 'redux-form';
import { connect } from 'react-redux';

// Components
import NumberControl from '../controls/NumberControl.react';
import InputField from '../configurable/InputField.react';
import CheckboxField from '../configurable/CheckboxField.react';

// Selectors
import { currentControl } from '../../selectors/control.selector';

// Utils
import { toNumber, toString } from '../../utils/normalize.util';


let NumberControlSettings = () => (
    <div>
        <div className="form-group row">
            <InputField type="text" name="label" text="Label name" component="input" placeholder="Enter label name" normalize={toString} />
        </div>
        <div className="form-group row">
            <InputField type="text" name="placeholder" text="Placeholder" component="input" placeholder="Enter placeholder" normalize={toString} />
        </div>
        <div className="form-group row">
            <InputField type="number" name="max" text="Maximum value" component="input" placeholder="Maximum" normalize={toNumber} />
        </div>
        <div className="form-group row">
            <InputField type="number" name="min" text="Minimum value" component="input" placeholder="Minimum" normalize={toNumber} />
        </div>
        <div className="form-group row">
            <InputField type="number" name="step" text="Increment" component="input" placeholder="Incremental value" normalize={toNumber} />
        </div>
        <div className="form-group">
            <CheckboxField name="disabled" text="Disabled" />
            <CheckboxField name="readonly" text="Read Only" />
            <CheckboxField name="required" text="Required" />
        </div>
    </div>
);


NumberControlSettings.propTypes = {};
NumberControlSettings.defaultProps = {};

NumberControlSettings = reduxForm({
    form: 'numberSettingsForm',
    fields: Object.keys(NumberControl.propTypes.settings),
})(NumberControlSettings);


NumberControlSettings = connect(
    state => ({
        initialValues: currentControl(state).settings,
    }),
)(NumberControlSettings);

export default NumberControlSettings;