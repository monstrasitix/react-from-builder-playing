// Core
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, initialize } from 'redux-form';
import { connect } from 'react-redux';

// Constants
import { autoCompleteOptions } from '../../constants/input.constant';

// Components
import TextControl from '../controls/TextControl.react';
import InputField from '../configurable/InputField.react';
import SelectField from '../configurable/SelectField.react';
import CheckboxField from '../configurable/CheckboxField.react';

// Selectors
import { currentControl } from '../../selectors/control.selector';



let CardNumberControlSettings = () => (
    <div>
        <div className="form-group row">
            <InputField
                type="text"
                name="label"
                text="Label name"
                component="input"
                placeholder="Label's name"
            />
        </div>

        <div className="form-group row">
            <InputField
                type="text"
                name="placeholder"
                text="Placeholder"
                component="input"
                placeholder="Text"
            />
        </div>

        <div className="form-group row">
            <InputField
                type="text"
                name="pattern"
                text="Pattern"
                component="input"
                placeholder="Pattern"
            />
        </div>

        <div className="form-group row">
            <InputField
                type="number"
                name="maxLength"
                text="Maximum length"
                component="input"
                placeholder="Maximum character length"
            />
        </div>

        <div className="form-group row">
            <InputField
                type="number"
                name="minLength"
                text="Minimum length"
                component="input"
                placeholder="Minimum character length"
            />
        </div>

        <div className="form-group row">
            <SelectField
                text="Autocomplete"
                name="autoComplete"
                options={autoCompleteOptions.values}
            />
        </div>

        <div className="form-group">
            <CheckboxField name="disabled" text="Disabled" />
            <CheckboxField name="readOnly" text="Read Only" />
            <CheckboxField name="required" text="Required" />
        </div>
    </div>
);


CardNumberControlSettings.propTypes = {};
CardNumberControlSettings.defaultProps = {};


CardNumberControlSettings = reduxForm({
    form: 'cardNumberSettingsForm',
    fields: [
        'label',
        'pattern',
        'readOnly',
        'disabled',
        'required',
        'maxLength',
        'minLength',
        'placeholder',
        'autoComplete',
    ],
})(CardNumberControlSettings);


CardNumberControlSettings = connect(
    state => ({
        initialValues: currentControl(state).settings,
    }),
)(CardNumberControlSettings);



export default CardNumberControlSettings;