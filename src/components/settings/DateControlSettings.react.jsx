// Core
import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, initialize } from 'redux-form';
import { connect } from 'react-redux';

// Components
import InputField from '../configurable/InputField.react';
// import SelectField from '../configurable/SelectField.react';
import CheckboxField from '../configurable/CheckboxField.react';

// Selectors
import { currentControl } from '../../selectors/control.selector';


let DateControlSettings = () => (
    <div>
        <div className="form-group row">
            <InputField
                type="text"
                name="label"
                text="Label name"
                component="input"
                placeholder="Enter label name"
            />
        </div>

        <div className="form-group row">
            <InputField
                type="text"
                name="placeholder"
                text="Placeholder"
                component="input"
                placeholder="Enter placeholder"
            />
        </div>

        <div className="form-group">
            <CheckboxField name="disabled" text="Disabled" />
            <CheckboxField name="readOnly" text="Read Only" />
            <CheckboxField name="required" text="Required" />
        </div>
    </div>
);


DateControlSettings.propTypes = {};
DateControlSettings.defaultProps = {};

DateControlSettings = reduxForm({
    form: 'dateSettingsForm',
    fields: [
        'label',
        'disabled',
        'readOnly',
        'required',
        'placeholder',
    ],
})(DateControlSettings);


DateControlSettings = connect(
    state => ({
        initialValues: currentControl(state).settings,
    }),
)(DateControlSettings);

export default DateControlSettings;