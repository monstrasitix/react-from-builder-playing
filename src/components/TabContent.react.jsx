// Core
// Core
import React from 'react';
import PropTypes from 'prop-types';

const TabContent = ({ id, isActive, children }) => (
    <div id={id} className={`tab-pane fade ${isActive ? 'show active' : ''}`}>
        {children}
    </div>
);


TabContent.propTypes = {
    isActive: PropTypes.bool,
    id: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
};


TabContent.defaultProps = {
    isActive: false,
};


export default TabContent;