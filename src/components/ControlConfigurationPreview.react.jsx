// Core
import React from 'react';
import PropTypes from 'prop-types';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

// Components
import Card from '../components/Card.react';
import Custom from '../components/Custom.react';
import Message from '../components/Message.react';
import { CONTROLS } from '../components/controls/controls.react';



export const DraggableSection = ({ control, onControlChange }) => (provided, snapshot) => (
    <div>
        <div
            className="container-fluid"
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
        >
            <div className="row">
                <div className="col-11">
                    <Custom
                        component={CONTROLS[control.type].control}
                        attributes={{
                            id: control.id,
                            settings: control.settings,
                        }}
                    />
                </div>

                <div className="col-1 d-flex align-items-center">
                    <button
                        type="button"
                        className="btn btn-sm btn-link"
                        onClick={() => onControlChange(control.id)}
                    >
                        <i className="fa fa-eye" />
                    </button>
                </div>
            </div>
        </div>
        {provided.placeholder}
    </div>
);


export const DraggableSectionMap = onControlChange => (control, index) => (
    <Draggable key={control.id} draggableId={control.id} index={index}>
        {DraggableSection({ control, onControlChange })}
    </Draggable>
);


export const DroppableSection = ({ createdControls, onControlChange }) => (provided, snapshot) => (
    <Card>
        <div ref={provided.innerRef}>
            <p className="lead mb-2">Customization Preview</p>
            <form>
                {
                    createdControls.length
                        ? createdControls.map(DraggableSectionMap(onControlChange))
                        : <Message message="No controls" />
                }
            </form>
        </div>
        {provided.placeholder}
    </Card>
);


const ControlConfigurationPreview = ({ createdControls, droppableId, type, onControlChange }) => (
    <Droppable type={type} droppableId={droppableId}>
        {DroppableSection({
            createdControls,
            onControlChange,
        })}
    </Droppable>
);


ControlConfigurationPreview.propTypes = {
    createdControls: PropTypes.array,
    type: PropTypes.string.isRequired,
    droppableId: PropTypes.string.isRequired,
    onControlChange: PropTypes.func.isRequired,
};


ControlConfigurationPreview.defaultProps = {
    createdControls: [],
};


export default ControlConfigurationPreview;