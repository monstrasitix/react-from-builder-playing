// Core
import React from 'react';
import PropTypes from 'prop-types';

// Components
import Card from './Card.react';
import Message from './Message.react';
import Custom from './Custom.react';
import { CONTROLS } from './controls/controls.react';


const ControlSettings = ({ currentControl, onChange }) => (
    currentControl.type !== undefined
        ? <Custom
            component={CONTROLS[currentControl.type].settingsControl}
            attributes={{ onChange, enableReinitialize: true }}
        />
        : <Message message="No control in use" />
);



ControlSettings.propTypes = {
    currentControl: PropTypes.object,
    onChange: PropTypes.func.isRequired,
};


ControlSettings.defaultProps = {
    currentControl: {},
};

export default ControlSettings;