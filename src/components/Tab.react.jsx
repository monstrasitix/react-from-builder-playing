// Core
import React from 'react';
import PropTypes from 'prop-types';


const Tab = ({ id, isActive, text }) => (
    <li className="nav-item">
        <a href={`#${id}`} className={`nav-link ${isActive ? 'active' : ''}`} data-toggle="tab">
            {text}
        </a>
    </li>
);


Tab.propTypes = {
    isActive: PropTypes.bool,
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
};


Tab.defaultProps = {
    isActive: false,
};


export default Tab;