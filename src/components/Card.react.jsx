// Core
import React from 'react';
import PropTypes from 'prop-types';


const Card = ({ classCard, classBody, children }) => (
    <div className={`card ${classCard}`}>
        <div className={`card-body ${classBody}`}>
            {children}
        </div>
    </div>
);


Card.propTypes = {
    children: PropTypes.node,
    classCard: PropTypes.string,
    classBody: PropTypes.string,
};


Card.defaultProps = {
    children: '',
    classCard: '',
    classBody: '',
};


export default Card;