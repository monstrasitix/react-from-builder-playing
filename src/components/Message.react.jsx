// Core
import React from 'react';
import PropTypes from 'prop-types';

// Components
import Card from './Card.react';


const Message = ({ message }) => (
    <Card classBody="bg-light text-center">
        {message}
    </Card>
);


Message.propTypes = {
    message: PropTypes.string.isRequired,
};


export default Message;