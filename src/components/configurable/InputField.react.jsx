// Core
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

const InputField = ({ text, type, component, name, placeholder, normalize }) => (
    <Fragment>
        <label className="col-sm-4 col-form-label" htmlFor={name}>
            {text}
        </label>
        <div className="col-sm-8">
            <Field
                id={name}
                type={type}
                name={name}
                component={component}
                normalize={normalize}
                className="form-control"
                placeholder={placeholder}
            />
        </div>
    </Fragment>
);


InputField.propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    normalize: PropTypes.func,
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    component: PropTypes.string.isRequired,
};


InputField.defaultProps = {
    text: 'Input field',
    type: 'text',
    placeholder: 'Input field',
    normalize: value => value,
};


export default InputField;