// Core
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

export const optionMapper = value => (
    <option key={value} value={value}>{value}</option>
);

const SelectField = ({ text, name, options }) => (
    <Fragment>
        <label className="col-sm-4 col-form-label" htmlFor={name}>
            {text}
        </label>
        <div className="col-sm-8">
            <Field
                id={name}
                name={name}
                component="select"
                className="form-control"
            >
                {options.map(optionMapper)}
            </Field>
        </div>
    </Fragment>
);


SelectField.propTypes = {
    text: PropTypes.string,
    options: PropTypes.array,
    name: PropTypes.string.isRequired,
};


SelectField.defaultProps = {
    options: [],
    text: 'Selection field',
};


export default SelectField;