// Core
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';


const CheckboxField = ({ name, text }) => (
    <div className="custom-control custom-checkbox">
        <Field
            id={name}
            name={name}
            type="checkbox"
            component="input"
            className="custom-control-input"
        />
        <label className="custom-control-label" htmlFor={name}>
            {text}
        </label>
    </div>
);

CheckboxField.propTypes = {
    text: PropTypes.string,
    name: PropTypes.string.isRequired,
};


CheckboxField.defaultProps = {
    text: 'Checkbox field',
};


export default CheckboxField;