// Core
import uuid from 'uuid';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Components
import Main from '../components/Main.react';
import { CONTROLS } from '../components/controls/controls.react';

// Containers
import ControlSettingsContainer from './ControlSettingsContainer.react';

// Selectors
import { created, available, currentControl } from '../selectors/control.selector';

// Actions
import {
    controlAdd,
    controlSort,
    controlRemove,
    controlReplace,
    controlCurrentUpdate,
} from '../actions/control.action';



export const onDragEnd = dispatch => ({ source, destination, draggableId }) => {
    if (destination) {
        switch (true) {
            case source.droppableId === 'AVAILABLE' && destination.droppableId === 'CREATED':
                const id = uuid();
                dispatch(controlAdd({
                    data: {
                        id,
                        type: draggableId,
                        settings: CONTROLS[draggableId].control.defaultProps.settings,
                    },
                    sourceIndex: source.index,
                    destinationIndex: destination.index,
                }));
                dispatch(controlCurrentUpdate(id));
                break;
            case source.droppableId === 'CREATED' && destination.droppableId === 'CREATED':
                dispatch(controlSort({
                    sourceIndex: source.index,
                    destinationIndex: destination.index,
                }));
                break;
        }
    } else {
        if (source.droppableId === 'CREATED') {
            dispatch(controlRemove(draggableId));
        }
    }
};


export const onControlChange = dispatch => (id) => dispatch(controlCurrentUpdate(id));


export const mapStateToProps = state => ({
    createdControls: created(state),
    currentControl: currentControl(state),
    availableControls: available(CONTROLS),
    tabs: [
        { id: 'tab_1', isActive: true, text: 'Available' },
        { id: 'tab_2', isActive: false, text: 'Configuration' },
    ],
    type: 'CONTROL',
    droppableIds: {
        created: 'CREATED',
        available: 'AVAILABLE',
    },
});


export const mapDispatchToProps = dispatch => ({
    onDragEnd: onDragEnd(dispatch),
    onControlChange: onControlChange(dispatch),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Main);