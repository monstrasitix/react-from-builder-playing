// Core
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Components
import ControlSettings from '../components/ControlSettings.react';

// Selectors
import { currentControl } from '../selectors/control.selector';

// Actions
import { controlUpdate } from '../actions/control.action';


export function onChange(dispatch, ownProps) {
    return values => dispatch(controlUpdate({
        values,
        id: ownProps.currentControl.id,
    }));
}


export const mapStateToProps = () => ({});

export const mapDispatchToProps = (dispatch, ownProps) => ({
    onChange: onChange(dispatch, ownProps),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ControlSettings);