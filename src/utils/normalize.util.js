export const toNumber = value => value && parseInt(value, 10);
export const toString = value => value && value.trim();