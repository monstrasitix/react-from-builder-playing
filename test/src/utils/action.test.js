// Core
import Chai from 'chai';

// Systems Under Test
import { createAction } from '../../../src/utils/action.util';


describe('Source/Utils/Action ->', () => {
    describe('createAction ->', () => {
        it('should be curried', () => {
            // Setup
            const type = 'type';
            // Execution
            const sut = createAction(type);
            // Assertions
            Chai.expect(sut.length).to.eq(1);
            Chai.expect(sut).to.be.a('Function');
        });


        it('should construct an object from 2 parameters', () => {
            // Setup
            const type = 'type';
            const payload = 'payload';
            // Execution
            const curried = createAction(type);
            const sut = curried(payload);
            // Assertions
            Chai.expect(sut).eql({ type, payload });
        });
    });
});