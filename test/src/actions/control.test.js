// Core
import Chai from 'chai';

// Systems Under Test
import {
    actions,
    controlAdd,
    controlSort,
    controlRemove,
    controlUpdate,
    controlReplace,
    controlCurrentUpdate,
} from '../../../src/actions/control.action';


describe('Source/Actions/Control ->', () => {
    describe('actions ->', () => {
        it('must be equal this value', () => {
            // Assertion
            Chai.expect(actions).to.eql({
                CONTROL_ADD: 'CONTROL_ADD',
                CONTROL_SORT: 'CONTROL_SORT',
                CONTROL_UPDATE: 'CONTROL_UPDATE',
                CONTROL_REMOVE: 'CONTROL_REMOVE',
                CONTROL_REPLACE: 'CONTROL_REPLACE',
                CONTROL_CURRENT_UPDATE: 'CONTROL_CURRENT_UPDATE',
            });
        });


        it('should have properties same as values', () => {
            // Setup
            const entries = Object.entries(actions);
            const resultMustBeTrue = result => result === true;
            const propertyValueComparison = ([property, value]) => property === value;
            // Execution
            const results = entries.map(propertyValueComparison);
            // Assertions
            Chai.expect(results.every(resultMustBeTrue)).to.eq(true);
        });
    });

    describe('Control Actions ->', () => {
        it('should return actions', () => {
            // Setup
            const actions2 = [
                controlAdd,
                controlSort,
                controlRemove,
                controlUpdate,
                controlReplace,
                controlCurrentUpdate,
            ].map(action => action('').type);
            // Assertions
            Chai.expect(actions2).to.eql([
                actions.CONTROL_ADD,
                actions.CONTROL_SORT,
                actions.CONTROL_REMOVE,
                actions.CONTROL_UPDATE,
                actions.CONTROL_REPLACE,
                actions.CONTROL_CURRENT_UPDATE,
            ]);
        });
    });
});