export const start = process =>
    `Running enviroment: ${process.env.NODE_ENV || "dev"}`

export const uncaughtException = logger => error =>
    logger('UncaughtException:', error.message)


export const unhandledRejection = logger => reason =>
    logger('UnhandledRejection:', reason)


export const initialize = async (server, { views, routes, registrations }) => {
    try {
        // server.views(views);
        server.route(routes);

        await server.register(registrations);
        await server.start();

        return server;
    } catch (error) {
        return error;
    }
};


export const success = logger => server =>
    logger('Server running at:', server.info.uri);


export const failure = logger => error =>
    logger(error);

export const SIGINT = process => () => process.exit(0);

export const SIGTERM = process => () => process.exit(0);