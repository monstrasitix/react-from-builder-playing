// Core
import Path from 'path';

export default {
    connection: {
        port: process.env.PORT || 8000,
        host: process.env.HOST || 'localhost',
        routes: {
            files: {
                relativeTo: Path.join(process.cwd(), './src')
            },
        },
    },
};