// Core
import Path from 'path';

// View Engines
import HapiReactViews from 'hapi-react-views';

export default {
    path: './src/containers',
    compileOptions: {
        doctype: '<!DOCTYPE html>',
        renderMethod: 'renderToString',
    },
    engines: {
        jsx: HapiReactViews,
    },
    relativeTo: Path.join(__dirname, '../'),
};