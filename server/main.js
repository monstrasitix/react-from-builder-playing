// Core
import Hapi from 'hapi';
import * as Server from './server';

// Configurations
import SERVER_CONFIG from './config/server.config';


import routes from './routes';
import registrations from './registrations';


console.log(Server.start(process));


process.on('SIGINT', Server.SIGINT(process));
process.on('SIGTERM', Server.SIGTERM(process));


process.on('uncaughtException', Server.uncaughtException(console.log));
process.on('unhandledRejection', Server.unhandledRejection(console.log));


Server.initialize(Hapi.server(SERVER_CONFIG.connection), {
    views: require('./views'),
    routes,
    registrations,
})
    .then(Server.success(console.log))
    .catch(Server.failure(console.log));
