// Registrations
import Inert from 'inert';
import Vision from 'vision';
import Blipp from 'blipp';
import Package from '../package.json';

const Swagger = {
    plugin: require('hapi-swagger'),
    options: {
        info: {
            title: 'Test API Documentation',
            version: Package.version,
        },
    },
};

export default [
    Inert,
    Blipp,
    Vision,
    Swagger,
];