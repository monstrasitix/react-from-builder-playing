export const getFiles = (request, h) =>
    h.file(request.params.path).vary('x-magic');