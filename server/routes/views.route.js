// Core
import Path from 'path';

// Handlers
import * as Handler from '../handlers/view.handler';


export default [
    {
        method: 'GET',
        path: '/{path*}',
        handler: Handler.getFiles,
    },
];