// Core
import React from 'react';
import ReactDOM from 'react-dom/server';

export default [
    {
        method: 'GET',
        path: '/render',
        handler: () => ReactDOM.renderToString(<h1>Some heading</h1>),
    }
];